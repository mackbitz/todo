# todo
Create e2e test for this TODO App using cypress (https://www.cypress.io/)
Test out the app and create as many tests as you want and think is necessary.
If you are more familiar with other testing framework, feel free to use it instead.
there is a simple test already added to /tests/e2e/specs

node.js and npm (https://nodejs.org/en/) are required to have installed to complete the task.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
